<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE OR REPLACE FUNCTION setcreated_at()
              RETURNS trigger AS
            $BODY$
              DECLARE
                    BEGIN
            NEW.created_at=now();
                    return NEW; 
                    end;   
            $BODY$
              LANGUAGE plpgsql VOLATILE STRICT
              COST 100;
            ALTER FUNCTION setcreated_at()
              OWNER TO postgres;
        CREATE OR REPLACE FUNCTION setupdated_at()
              RETURNS trigger AS
            $BODY$
              DECLARE
                    BEGIN         
            NEW.updated_at=now();
                    return NEW; 
                    end;   
            $BODY$
              LANGUAGE plpgsql VOLATILE STRICT
              COST 100;
            ALTER FUNCTION setupdated_at()
              OWNER TO postgres;

        CREATE TRIGGER update_modified
              BEFORE UPDATE
              ON users
              FOR EACH ROW
              EXECUTE PROCEDURE setupdated_at();
              
        CREATE TRIGGER created
              BEFORE INSERT
              ON users
              FOR EACH ROW
              EXECUTE PROCEDURE setcreated_at();
        CREATE TRIGGER update_modified
              BEFORE UPDATE
              ON salaries
              FOR EACH ROW
              EXECUTE PROCEDURE setupdated_at();
              
        CREATE TRIGGER created
              BEFORE INSERT
              ON salaries
              FOR EACH ROW
              EXECUTE PROCEDURE setcreated_at();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('
          DROP TRIGGER IF EXISTS setcreated_at ON users CASCADE;
          DROP TRIGGER IF EXISTS setcreated_at ON salaries CASCADE;
          DROP TRIGGER IF EXISTS setupdated_at ON users CASCADE;
          DROP TRIGGER IF EXISTS setupdated_at ON salaries CASCADE;

          ');
    }
}
