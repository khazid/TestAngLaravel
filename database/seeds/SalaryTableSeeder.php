<?php

use Illuminate\Database\Seeder;

class SalaryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salaries')->insert(['users_id' => 1,'amount' => 25500,]);
        DB::table('salaries')->insert(['users_id' => 2,'amount' => 83300,]);
        DB::table('salaries')->insert(['users_id' => 3,'amount' => 50500,]);
        DB::table('salaries')->insert(['users_id' => 4,'amount' => 30000,]);
    }
}
