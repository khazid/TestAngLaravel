//***** FUNCIONES ANGULAR PARA EL PROCESAMIENTO DE DATA ****

app.controller('HomeController', function($scope,$http){
  $scope.pools = [];
});

//***CONTROLADOR DE INFORMACION DE USUARIOS Y SALARIOS****

app.controller('SalaryController', function(dataFactory,$scope,$http){
  $scope.data = [];//DATA PARA LA LISTA
  $scope.libraryTemp = {}; //DATA TEMPORAL PARA EL MANEJO DE SEARCH
  $scope.totalItemsTemp = {}; // TOTAL DE ITEMS PARA PAGINACION TEMPORAL
  $scope.totalItems = 0;// TOTAL DE ITEMS PARA PAGINACION
  
  $scope.pageChanged = function(newPage) {
    getResultsPage(newPage);// MANEJO DE VISTAS PARA LA PAGINACION CUANDO SE CAMBIA DE PAGINA 
  };
  getResultsPage(1);//OBTENER LA PRIMERA PAGINA PARA PAGINACION
 
  //*** FUNCION PARA SEARCH DEL LISTADO DINAMICO ***
  function getResultsPage(pageNumber) { 
      if(! $.isEmptyObject($scope.libraryTemp)){
          dataFactory.httpRequest('salary?search='+$scope.searchText+'&page='+pageNumber).then(function(data) {
            $scope.data = data.data;
            $scope.totalItems = data.total;
          });
      }else{
        dataFactory.httpRequest('salary?page='+pageNumber).then(function(data) {
          $scope.data = data.data;
          $scope.totalItems = data.total;
        });
      }
  }

  //***SCOPE PARA LA BUSQUEDA EN LA LISTA
  $scope.searchDB = function(){
      if($scope.searchText.length >= 3){//A PARTIR DE QUE SE INGRESA EL 3 CARACTER ES QUE SE INICIA LA BUSQUEDA
          if($.isEmptyObject($scope.libraryTemp)){
              $scope.libraryTemp = $scope.data;
              $scope.totalItemsTemp = $scope.totalItems;
              $scope.data = {};
          }
          getResultsPage(1);
      }else{
          if(! $.isEmptyObject($scope.libraryTemp)){
              $scope.data = $scope.libraryTemp ;
              $scope.totalItems = $scope.totalItemsTemp;
              $scope.libraryTemp = {};
          }
      }
  }

  //****SCOPE PARA AGREGAR UN NUEVO USUARIO ***
  $scope.saveAdd = function(){
    dataFactory.httpRequest('salary','POST',{},$scope.form).then(function(data) {
      $scope.data.push(data); //ENVIO DE LA DATA AL CONTROLADOR LARAVEL
      $(".modal").modal("hide");
    });
  }

  //***SCOPE PARA LA EDICION DE DATA 
  $scope.edit = function(id){
    dataFactory.httpRequest('salary/'+id+'/edit').then(function(data) {
    	console.log(data); //PARA VERIFICAR LOS DATOS A MODIFICAR FUERON LOS SELECCIONADOS
      	$scope.form = data; // ENVIA LOS DATOS AL FORMULARIO DENTRO DEL MODAL PARA PODER MODIFICARLOS
    });
  }

  //** SCOPE PARA GUARDAR LOS DATOS MODIFICADOS
  $scope.saveEdit = function(){
    dataFactory.httpRequest('salary/'+$scope.form.id,'PUT',{},$scope.form).then(function(data) {
      	$(".modal").modal("hide");
        $scope.data = apiModifyTable($scope.data,data.id,data);//SOLICITUD DE MODIFICACION DE TABLA ENVIADA AL CONTROLADOR LARAVEL
    });
  }

  //** SCOPE PARA ELIMINAR UN REGISTRO 
  $scope.remove = function(item,index){
    var result = confirm("Seguro que desa eliminar este item?");
   	if (result) {
      dataFactory.httpRequest('salary/'+item,'DELETE').then(function(data) {
          $scope.data.splice(index,1);//QUITA DE LA LISTA EL REGISTRO 
      });
    }
  }
});