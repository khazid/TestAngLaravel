var app =  angular.module('main-App',['ngRoute','angularUtils.directives.dirPagination']);
app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'templates/salary.html',
                controller: 'SalaryController'
            }).
            when('/salary', {
                templateUrl: 'templates/salary.html',
                controller: 'SalaryController'
            });
}]);

