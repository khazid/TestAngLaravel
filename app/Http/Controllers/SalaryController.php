<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Salary;

class SalaryController extends Controller
{
    /*
        Cada Modelo esta relacionado en su definicion utilizando la funcion HasOne de Eloquent
        La data obtenida es procesada a traves de la vista por medio de Angular utilizando el archivo Public/app/controller/salaryController.js
    */

    public function index(Request $request)
    {
        $input = $request->all();

        //si se esta accesando al listado a traves de un search
        if($request->get('search')){
            $items = User::join('salaries','salaries.users_id','=','users.id')
                ->select('users.name','users.email','users.id as users_id','salaries.amount')
            	->where("name", "LIKE", "%{$request->get('search')}%")
            	->paginate(5);
                
        }else{
        // si se esta accesando al listado por defecto
            $items=User::join('salaries','salaries.users_id','=','users.id')
                        ->select('users.name','users.email','users.id as users_id','salaries.amount')
                        ->paginate(5);
		      
        }
        return response($items);  
        
    }
    public function store(Request $request)
    {
    	//Guardar datos para el usuario
        $create = new User;
        $create->name=$request->name;
        $create->email=$request->email;
        $create->password=bcrypt(str_random(10));
        if($create->save()){
        	//Guardar datos del salario
        	$newsal = new Salary;
        	$newsal->users_id=$create->id;
        	$newsal->amount=$request->amount;
        	$newsal->save();
        }else{
        	return response('Se genero un error al intentar guardar los datos');
        }

        return response($create);
    }
    public function edit($id)
    {
        $item = User::find($id);
        $salario=Salary::where('users_id','=',$id)->get();
        $item['amount']=$salario[0]->amount;
        return response($item);
    }

    //Actualizar datos del usuario y el salario
    public function update(Request $request,$id)
    {
    	$input = $request->all();
        $item = User::find($id);
        $item->name=$request->name;
        $item->email= $request->email;
        
        if($item->save()){
            //actualizacion de datos del salario
            $salario = Salary::where('users_id','=',$item->id)->first();
            $salario->amount=$request->amount;
            $salario->save();

             return response('Actualizacion Exitosa');
        }else{
             return response('Se genero un error al intentar Actualizar los datos');
        }
       
    }
    public function destroy($id)
    {   //Se eliminan los datos relacionados den cada tabla
        $salario = Salary::where('users_id','=',$id)->first();
        $salario->delete();
        $user = User::find($id);
        $user->delete();
        
         return response('Eliminacion del usuario exitosa');
    }
}
