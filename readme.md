## Lista de Usuarios y Salarios (Laravel 5.2 con AngularJS)
Se utilizo:

Laravel 5.2
AngularJs
PostgreSQL
Bootstrap

## Instalacion

1) Para la utilizacion de este Demo se debe crear una base de datos con el nombre "test".

2) Se debe realizar la migracion de las tablas correspondientes utilizando el comando desde la consola y despues de haber ingresado a la carpeta del proyecto:
php artisan migrate:refresh --seed

Este comando creara las tablas y llenara las mismas con la data dummy para el proyecto.
Adicionalmente se generan los triggers necesarios para los campos create_at, update_at los cuales estan contenidas en el archivo de migracion create_trigger.php y los mismos estan basados en PostgreSQL.

3)Modificar el archivo .env ubicado en la raiz del proyecto con las credenciales de la Base de Datos.
Ejemplo:
DB_HOST=localhost
DB_DATABASE=test
DB_USERNAME=postgres
DB_PASSWORD=postgresql

## Estructura Vistas

Los archivos de las vistas se encuentran en la carpeta Resources/views, se generaron en formato html para ser cargados desde angular y los mismos estan estructurados de la siguiente forma:

Templates/ 
Templates/salary.html: vista que muestra el listado de usuarios y salarios.
Templates/home.html: vista de bienvenida de la aplicacion utilizada en el boton Test del menu principal del aplicacion.
Templates/dirPagination.html: vista que contiene la paginacion utilizada en la lista de usuarios.

app.blade: vista principal que contiene el header HTML y la estructura principal a usar en las vistas. (las vistas contenedoras, como salary.html, son llamadas a traves de esta vista usando angular y la etiqueta <ng-view></ng-view>)



## Estructura Angular Frontend

Los archivos de angular pueden ser encontrados en :
public/app/
public/app/controllers/SalaryController.js: controlador angular de la data procesada por la vista.  
public/app/helper/helper.js: funciones de ayuda del angular.
public/app/package/dirPagination.js: libreria de paginacion de lista.
public/app/services/services.js: servicios de manejo de request
public/app/route.js: definicion de rutas para el laravel desde angular. 


## Estructura Controllador Laravel 

SalaryController.php es el controlador utiizado para procesar la data del lado del servidor, en el se encuentran definidas todas la funciones CRUD del sistema. 





